const express = require( 'express' );
const app = express();

const PORT = process.env.PORT || 3000;

const server = app.listen( PORT, () => {
    console.log( 'Server Port '+PORT );
} );

app.use( express.static( 'www' ) );

const socket = require( 'socket.io' );
const io = socket( server );

io.on( 'connection', sock => {
    console.log( 'neue Verbindung: ' + sock.id );

    //sock.emit( 'allesok', 'OK' );

    sock.on( 'neueruser', username => {
        // sock.emit( 'user', username ); // nur auf dem socket von dem aus die Verbindung aufgebaut wurde
        io.emit( 'user', username + ' ' + sock.id ); // alle sockets!
        //sock.broadcast.emit( 'user', '...'); // alle außer mir selbst

    })

    sock.on( 'disconnect', reason => {
        console.log( 'weg: ' + sock.id)
        io.emit( 'user', 'einer weniger: '+ sock.id  );
    })


});